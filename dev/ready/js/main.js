
jQuery(window).on('load', function () {
	var $pannel = $('.panel-comparison');
	if ($pannel.hasClass('show')) {
		$pannel.show();
	}
});

//-----  isMobile  -----//
var isMobile = { Android: function () { return navigator.userAgent.match(/Android/i) }, BlackBerry: function () { return navigator.userAgent.match(/BlackBerry/i) }, iOS: function () { return navigator.userAgent.match(/iPhone|iPad|iPod/i) }, Opera: function () { return navigator.userAgent.match(/Opera Mini/i) }, Windows: function () { return navigator.userAgent.match(/IEMobile/i) }, any: function () { return isMobile.Android() || isMobile.BlackBerry() || isMobile.iOS() || isMobile.Opera() || isMobile.Windows() } };


$(document).ready(function() {

	//Chrome Smooth Scroll
	try {
		$.browserSelector();
		if($("html").hasClass("chrome")) {
			$.smoothScroll();
		}
	} catch(err) {

	};

	$('.main-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		dots: true,
		arrows: false
	});
	$('.context-slider__img').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: true,
		autoplaySpeed: 5000,
		dots: true,
		arrows: false
	});
	$('.manufacturers-slider').slick({
		infinite: false,
		slidesToShow: 6,
		slidesToScroll: 1,
		autoplaySpeed: 5000,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});
	$('.catalog-slider').slick({
		infinite: false,
		slidesToShow: 4,
		slidesToScroll: 1,
		autoplaySpeed: 5000,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1240,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 600,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.panel-comparison__list').slick({
		infinite: false,
		slidesToShow: 7,
		slidesToScroll: 1,
		autoplaySpeed: 5000,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1240,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 650,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 480,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			}
			,
			{
				breakpoint: 380,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
		]
	});

	$('.dots-slider').slick({
		infinite: false,
		slidesToShow: 5,
		vertical: true,
		slidesToScroll: 1,
		autoplaySpeed: 5000,
		dots: false,
		arrows: true,
		responsive: [
			{
				breakpoint: 1240,
				settings: {
					slidesToShow: 5,
					slidesToScroll: 1,
				}
			},
			{
				breakpoint: 992,
				settings: {
					slidesToShow: 4,
					slidesToScroll: 2
				}
			},
			{
				breakpoint: 461,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1,
					vertical: false,
				}
			}
		]
	});

	$(function () {
		

		$(".mobile-filter-link a").click(function () {
			$(this).toggleClass("on");
			$(".sidebar-filter-block").addClass("active");
			$("body").addClass("no-scroll");
			$(".menu-overlay").addClass("show");
			return false;
		});
		$(".pannel-close").click(function (e) {
			e.preventDefault();
			$(".sidebar-filter-block").removeClass("active");
			$("body").removeClass("no-scroll");
			$(".menu-overlay").removeClass("show");
			return false;
		});
		$(".menu-overlay").click(function () {
			$(".sidebar-filter-block").removeClass("active");
			$("body").removeClass("no-scroll");
			$(".menu-overlay").removeClass("show");
			return false;
		});

	});


	$(function () {
		

		$(".panel-comparison__show").click(function () {
			$(this).parent().parent().toggleClass('active');
			return false;
		});

	});

	$(".popup-link").fancybox({
		'speedIn': 500,
		'speedOut': 400,
		'padding': 0,
		'helpers': {
			'overlay': { 'locked': false }
		},
		'touch': false
	});

	$(".scroll-panel__menu-burger").click(function () {
		$(this).toggleClass("on");
		$(this).children(".scroll-panel__menu-wrap").toggleClass('show');
		return false;
	});

	$(function () {
		var headerH = $("#js-header").height() + 100;
		var navH = $("#js-scroll-panel").innerHeight();

		$(document).on("scroll", function () {
			var documentScroll = $(this).scrollTop();
			if (documentScroll > headerH) {
				$("#js-scroll-panel").addClass("is-fixed");
			}
			else {
				$("#js-scroll-panel").removeClass("is-fixed");
				$('.scroll-panel__search .scroll-panel__autocomplete').removeClass('is-active');
			}
		});
	});

	/*** загрушка для поиска ***/
	$('html').on('focus', '.head-search .search-block .search-block__input', function () {
		$('.head-search .search-block .search-block__autocomplete').addClass('is-active');
	});
	$('html').on('click', function (e) {
		if (!$(e.target).closest('.head-search .search-block').length)
			$('.head-search .search-block .search-block__autocomplete').removeClass('is-active');
	});

	$('html').on('focus', '.scroll-panel__search .scroll-panel__search-input', function () {
		$('.scroll-panel__search .scroll-panel__autocomplete').addClass('is-active');
	});
	$('html').on('click', function (e) {
		if (!$(e.target).closest('.scroll-panel__search').length)
			$('.scroll-panel__search .scroll-panel__autocomplete').removeClass('is-active');
	});

	$(function () {

		var tabs = $('.home-goods__nav .container');
		var hash = $.trim(window.location.hash);

		if (hash) {
			$('.home-goods__tabs .home-goods__tab').removeClass('active');
			$('.home-goods__tabs ' + hash + '').addClass('active');
			$('.home-goods__nav .container .home-goods__nav__item').removeClass('active');
			$('.home-goods__nav .container .home-goods__nav__item a[href$="' + hash + '"]').parent().addClass('active');
		};

		tabs.each(function (i) {

			//Get all tabs
			var tab = $(this).find('> .home-goods__nav__item > a');

			tab.click(function (e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if (contentLocation.charAt(0) == "#") {

					e.preventDefault();

					//Make Tab Active
					tab.parent().removeClass('active');
					$(this).parent().addClass('active');
					window.location.hash = this.hash;

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');

				}

			});

		});

	});

	$(function () {

		var tabs = $('.page-publications__nav .container');
		var hash = $.trim(window.location.hash);

		if (hash) {
			$('.page-publications__tabs .page-publications__tab').removeClass('active');
			$('.page-publications__tabs ' + hash + '').addClass('active');
			$('.page-publications__nav .container .page-publications__nav-item').removeClass('active');
			$('.page-publications__nav .container .page-publications__nav-item a[href$="' + hash + '"]').parent().addClass('active');
		};

		tabs.each(function (i) {

			//Get all tabs
			var tab = $(this).find('> .page-publications__nav-item > a');

			tab.click(function (e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if (contentLocation.charAt(0) == "#") {

					e.preventDefault();

					//Make Tab Active
					tab.parent().removeClass('active');
					$(this).parent().addClass('active');
					window.location.hash = this.hash;

					//Show Tab Content & add active class
					$(contentLocation).show().addClass('active').siblings().hide().removeClass('active');

				}

			});

		});

	});

	if (!isMobile.any()) {
		$("input[name='phone'], input[type='tel']").mask("+7 (999) 999-99-99", {
			completed: function () { $(this).attr('data-valid', 'true'); }
		});
	}

	$(".login-registration__tab").not(":first").hide();
	$(".login-registration .login-registration__nav-item").click(function () {
		$(".login-registration .login-registration__nav-item").removeClass("active").eq($(this).index()).addClass("active");
		$(".login-registration__tab").hide().eq($(this).index()).fadeIn()
	}).eq(0).addClass("active");

	$(".text-unfolding__btn").click(function () {
		$(this).toggleClass("on");
		if ($(this).hasClass("on")) {
			$(this).text("Свернуть");
		}else {
			$(this).text("Показать");
		}
		
		$(this).parent().toggleClass("show");
	});

	$(function () {
		$('select').selectric();
	});

	$('select.custom-ico').selectric({
		optionsItemBuilder: function (itemData) {
			return itemData.text + (itemData.value.length ? '<span class="ico ico-' + itemData.value + '"></span>' : '');
		},
		labelBuilder: function (currItem) {
			return currItem.text + (currItem.value.length ? '<span class="ico ico-' + currItem.value + '"></span>' : '');
		}
	});

	$('select.quick-search__filter-step1').change(function (e) {
		$('select.quick-search__filter-step2').attr("disabled", false);
		$('select.quick-search__filter-step2').selectric('refresh');
	});
	$('select.quick-search__filter-step2').change(function (e) {
		$('select.quick-search__filter-step3').attr("disabled", false);
		$('select.quick-search__filter-step3').selectric('refresh');
	});
	$('select.quick-search__filter-step3').change(function (e) {
		$('.quick-search__filter-step4').css("display", "inline-block");
	});

	$(function () {
		$('.quantity__btn-plus').click(function (e) {
			e.preventDefault();
			fieldName = $(this).parent().children('.quantity__input');
			var currentVal = parseInt($(fieldName).val());
			if (!isNaN(currentVal)) {
				$(fieldName).val(currentVal + 1);
			} else {
				$(fieldName).val(0);
			}
		});
		$(".quantity__btn-minus").click(function (e) {
			e.preventDefault();
			fieldName = $(this).parent().children('.quantity__input');
			var currentVal = parseInt($(fieldName).val());
			if (!isNaN(currentVal) && currentVal > 0) {
				$(fieldName).val(currentVal - 1);
			} else {
				$(fieldName).val(0);
			}
		});
	});

	$(function () {

		var tabs = $('.product-page__tabNav');
		var hash = $.trim(window.location.hash);

		if (hash) {
			$('.product-page__tabs .product-page__tab').removeClass('active');
			$('.product-page__tabs ' + hash + '').addClass('active');
			$('.product-page__tabNav .product-page__tabNav-item').removeClass('active');
			$('.product-page__tabNav .product-page__tabNav-item a[href$="' + hash + '"]').parent().addClass('active');
		};

		tabs.each(function (i) {

			//Get all tabs
			var tab = $(this).find('> .product-page__tabNav-item > a');

			tab.click(function (e) {

				//Get Location of tab's content
				var contentLocation = $(this).attr('href');

				//Let go if not a hashed one
				if (contentLocation.charAt(0) == "#") {

					e.preventDefault();

					//Make Tab Active
					tab.parent().removeClass('active');
					$(this).parent().addClass('active');
					window.location.hash = this.hash;

					//Show Tab Content & add active class
					$(contentLocation).addClass('active').siblings().removeClass('active');

				}

			});

		});

	});

	$(function () {

		var dots = $('.product-gallery__dots');

		dots.each(function (i) {

			//Get all dots
			var dot = $(this).find('> .slick-list .product-gallery__dot');

			dot.click(function (e) {

				//Get Location of tab's content
				var contentLocation = $(this).data('miniature');

				// console.log(contentLocation);

					e.preventDefault();

					//Make Tab Active
				dot.removeClass('curent');
				$(this).addClass('curent');

				$(this).parents(".product-gallery").children('.product-gallery__big').children('.product-gallery__big-list').children('.product-gallery__big-item[data-fullimg="' + contentLocation + '"]').show().addClass('curent').siblings().hide().removeClass('curent');

			});

		});

	});

	$(function () {

		$('.accordion-nav').click(function (e) {
			e.preventDefault();
			$(this).parent().addClass('active').siblings().removeClass('active');

		});
	});

	$(function () {

		$('.sidebar-filter__head').click(function (e) {
			e.preventDefault();
			$(this).parent().toggleClass('active');
		});
	});

	$(function () {
		var sledaer = $(".filter-interval .slider-range_input")
		sledaer.slider({
			range: true,
			min: sledaer.data("min"),
			max: sledaer.data("max"),
			step: sledaer.data("step"),
			values: [0, 5],
			slide: function (event, ui) {
				$(this).parent().children('.range-controls').children('.slider_opt_txtbox1').val(ui.values[0]);
				$(this).parent().children('.range-controls').children('.slider_opt_txtbox2').val(ui.values[1]);
				console.log($(this).data("min"))
			}
			
		});
	

	
	});

	$('#goods_table').DataTable({
		scrollX: true,
		scrollCollapse: true,
		paging: false,
		searching: false,
		info: false,
		columns: [
			{ "width": "100px" },
			{ "width": "150px" },
			{ "width": "auto" },
			{ "width": "auto" },
			{ "width": "auto" },
			{ "width": "auto" },
			{ "width": "auto" },
			{ "width": "auto" },
			{ "width": "auto" }
		],
		fixedColumns: {
			leftColumns: 2
		},
		
		"order": [[2, 'asc']],
		columnDefs: [
			{ "orderable": false, "targets": 0 }
		]
		// "fnInitComplete": function () {
		// 	// custom scroll bars
		// 	$('.dataTables_scroll')
		// 		.mCustomScrollbar({
		// 			axis: "x",
		// 			scrollInertia: 0
		// 		});
		// }
	});
	$('#table_comparison').DataTable({
		scrollX: true,
		scrollCollapse: true,
		paging: false,
		searching: false,
		info: false,
		ordering: false,
		fixedColumns: {
			leftColumns: 1
		},
		dom: 'Bfrtip',
		buttons: [
			{
				extend: 'print',
				// autoPrint: false,
				text: 'Распечатать',
				title: '',
				messageTop: function () {

					return '<div class="print-header"><div class="print-header__logo"><img src="img/logo.png" alt=""></div><div class="print-header__contact"><div class="print-header__phone"><img src="img/phone.svg" alt=""><a class="head-phone__item" href="tel:+74955404800">+7 (495) 540-48-00</a><a class="head-phone__item" href="tel:+78003331353">+7 (800) 333-13-53</a></div><div class="print-header__email"><img src="img/email.svg" alt=""><a class="head-email__item" href="mailto:info@sensoren.ru">info@sensoren.ru</a></div></div></div>';
				},
				exportOptions: {
					modifier: {
						page: 'current'
					}
				}
			}
		]
	});

	$('select.documentation-lang__select').change(function (e) {
		$(this).parent().parent().parent().children('.documentation-lang__link').attr("href", $(this).val() );
	});


	$(".site-map__level-drop").click(function () {
		$(this).toggleClass("on");
		$(this).next(".site-map__level-sub").toggleClass("active");
		return false;
	});
	

});


